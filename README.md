# Projeto: Fonte de Tensão (SSC0180 - Eletrônica para Computação)

## Introdução:

O objetivo desse projeto é criar uma Fonte de Tensão ajustável entre 3V a 12V com capacidade de 100mA.


## Componentes

### Tabela de preços dos componentes escolhidos
| Componente | Quantidade | Preço(julho/2021) | Custo total |
| ------ | ------ | ------ | ------ |
| Capacitor Eletrolítico | 1 | [R$ 1,23](https://proesi.com.br/capacitor-eletrolitico-epcos-680uf-25v.html) | R$ 1,23 * 1 = R$ 1,23 |
| Chave HH | 1 | [R$ 0,52](https://www.baudaeletronica.com.br/chave-ss-ss12f23.html) | R$ 0,52 * 1 = R$ 0,52 |
| Diodo 1N4148 | 4 | [R$ 0,10](https://www.baudaeletronica.com.br/diodo-1n4148.html) | R$ 0,10 * 4 = R$ 0,40 |
| Diodo Zener BZX55C | 1 | [R$ 0,09](https://www.baudaeletronica.com.br/diodo-zener-bzx55c-13v-0-5w.html) | R$ 0,09 * 1 = R$ 0,09 |
| Fusível de Vidro | 1 | [R$ 0,19](https://www.baudaeletronica.com.br/fusivel-de-vidro-5x20-1-25a-250v.html) | R$ 0,19 * 1 = R$ 0,19 |
| Potenciômetro | 1 | [R$ 2,21](https://www.baudaeletronica.com.br/potenciometro-linear-de-5k-5000.html) | R$ 2,21 * 1 = R$ 2,21 |
| Resistor 120R | 1 | [R$ 0,20](https://www.baudaeletronica.com.br/resistor-150r-5-1w.html) | R$ 0,20 * 1 = R$ 0,20 |
| Resistor 2K2 | 1 | [R$ 0,06](https://www.baudaeletronica.com.br/resistor-2k2-5-1-4w.html) | R$ 0,06 * 1 = R$ 0,06 |
| Transformador | 1 | [R$ 24,97](https://www.baudaeletronica.com.br/transformador-trafo-12v-12v-200ma-110-220vac.html) | R$ 24,97 * 1 = R$ 24,97 |
| Transistor | 1 | [R$ 0,22](https://www.baudaeletronica.com.br/transistor-npn-bc337.html) | R$ 0,22 * 1 = R$ 0,22 |

**Custo total do projeto: R$29,69**

**Sites usados para a pesquisa de preços:**

[Bau da Eletrônica](https://www.baudaeletronica.com.br/)

[Proesi](https://proesi.com.br/)

### Componentes escolhidos

**Chave:** A chave é uma peça simples, ela conecta a fonte ao circuito, como o interruptor de uma lâmpada, quando "desligada" a energia não entra no circuito, quando "ligada" a energia entra no circuito.

**Fusível:** O fusível é um dispositivo de segurança que protege o circuito de picos de energia que danificariam as peças internas. O fusível tem um fio feito de metais de baixo ponto de fusão e permite apenas uma carga elétrica de até certo limite, caso uma carga maior passe pelo fusível, o fio se rompe, cortando a conexão entre a fonte e o circuito, assim o pico de energia não entra no circuito e não o danifica.

**Transformador:** O transformador é um dispositivo serve para alterar os valores de tensão e de corrente da energia recebida. Ele consiste em 2 fios, um primário e um secundário, enrolados em um núcleo metálico. A corrente elétrico que entra no fio primário causa a formação de uma outra corrente elétrica no fio secundário, tal corrente pode ter mais ou menos tensão e corrente dependendo do número de voltas dos fios. Pode-se dizer que o transformador "transforma" tensão em corrente e vice-versa, pois o produto de ambos contínua o mesmo em ambos os fios.

**Diodo:** Um diodo é uma peça que funciona como uma válvula. A válvula permite a passagem de um corrente de água em apenas um sentido, assim também é o diodo, mas com uma corrente elétrica. O diodo só permite que a corrente elétrica vá para a direção indicada. 

**Ponte de diodos:** A ponte de diodos consiste em quatro diodos conectados de forma que ele retificam a correnta, ou seja, eles impedem que haja cargas negativas na corrente. Isso ocorre ao custo de uma pequena parte da voltagem da corrente.

**Capacitor:** O capacitor é um dispostivo cuja função é armazenar cargas elétricas. Sua função mais fundamental é armazenar cargas elétricas para descarregá-las quando for necessário. O capacitor pode servir como temporizador, estabilizador ou regulador. As duas últimas funções descrevem melhor o trabalho dele em nosso circuito.

**Diodo Zener:** O diodo zener também é um diodo como o diodo comum, mas ele tem algumas peculiaridades. Todo diodo tem um limite de tensão que é capaz de barrar, geralmente esses limites são tão altos que são irrelevantes para um projeto. No entanto, o diodo zener tem um limite bem menor que varia de acordo com suas especificações. Caso a tensão seja maior do que o limite do diodo zener, parte da corrente passará por ele até que seja atingido o limite. Isso geralmente é utilizado como limitador de tensão nos circuitos, pois caso haja mais tensão do que o que o zener é capaz de barrar, parte dela passará e parte ficará.

**Resistor:** A função do resistor é limitar o fluxo de corrente em um circuito por dissipação. Em outras palavras, a função dele é diminuir a corrente elétrica que passa por ele, dissipando o que é "retido" em calor.

**Potênciometro:** O potenciômetro é um componente cuja função é limitar o fluxo de corrente no circuito, assim como um resistor. A diferença é que o potenciômetro pode ter sua resistência ajustada pelo seu eixo de giro para permitir que mais ou menos corrente passe. Em nosso circuito, ele limita a corrente entre 3V e 12V. 

**Transistor:** O transistor tem a função a função de uma chave, ele conecta o cátodo e o ânodo caso haja carga passando em sua base.


## Imagens do Circuito

### Circuito no Falstad

<img src = "./Imagens/Circuito_Falstad.png">

[Link do Circuito no Falstad](https://tinyurl.com/ygarwdvy)

### Esquema no Eagle

<img src="/Imagens/schematic_board.png">

### PCB no Eagle

<img src="/Imagens/board_pcb_eagle.png">


## Cálculos

### Cálculo do Transformador

O nosso circuito funciona com uma entrada média de 127V, ou seja, tendo uma tomada como fonte. A saída média esperada seria próxima de 12V, uma proporção de aproximadamente 10% da entrada. Entretanto devemos usar a entrada de pico para os cálculos. Primeiramente encontramos o valor de pico:

<img src="/Imagens/Vmax.png">

Sunstituindo ficamos com:

<img src="/Imagens/180aprox.png">

Em seguida, aplicando a proporção de 10%, obtemos que a saída de pico será 18V. Como o Falstad considera 1 = 100%, 10% da entrada será 0.1. No seguinte link temos a prática disso: https://tinyurl.com/yj22yonr

O "ratio" do transformador está configurado para 10% e com isso obtemos uma saída de pico de aproximadamente 18V. Isso também pode ser representado pela seguinte imagem:

<img src="/Imagens/Grafico_de_ondas_do_transformador.png">

Na esquerda temos a onda de saída, na direita a onda da fonte.

### Cálculo da Ponte de Diodos

A ponte de diodos é montada em dois pares, cada par conduz a corretnte a cada ciclo.

Ciclo 1:

<img src="/Imagens/Ponte_de_diodos_estado_1.png">

Ciclo 2:

<img src="/Imagens/Ponte_de_diodos_estado_2.png">

[Representação desses ciclos no Falstad.](https://tinyurl.com/yjhgkfoj)

A frequência é dada como a quantidade de vezes que a onda volta ao estado de pico em um segundo. Como a ponte faz com que ambos os ciclos tenham ondas positivas, isso significa que a frequência é dobrada, passando de 60hz para 120hz.

Frequência original:

<img src="/Imagens/Frequencia_1.png">

Frequência após a ponte:

<img src="/Imagens/Frequencia_2.png">

Quando uma corrente passa por um dioda, ocorre a perda de parte da tensão. Em um diodo de silício essa perda pode variar entre 0.6V e 0.7V. Para esse circuito, consideraremos a perda como sendo 0.7V. Cada ciclo da corrente passa por dois diodos, ou seja, perde 1.4V. 

Consideremos **Vf** como voltagem de funcionamento e **Vs** como voltagem de saída. O seguinte cálculo nos dá a voltagem de funcionamento do circuito:

<img src="/Imagens/Vf.png">

Sunstituindo pelos valores das variáveis, ficamos com:

<img src="/Imagens/16.6V.png">

### Cálculo do Resistor do Diodo Zener

Sabendo que a potência que o Zener escolhido deve suportar é de no máximo 400 mW (usaremos um de 500 mW por segurança), devemos fazer o seguinte cálculo para descobrir qual é a resistência que vamos colocar:

Calculando a corrente do zener:

<img src="/Imagens/calculo_resistor_zener.png">

Encontrando o valor do resistor:

<img src="/Imagens/valores_calculo_zener.png">

<img src="/Imagens/resultado_resistor_zener.png">

### Cálculo do Capacitor

Temos que:

<img src="/Imagens/Vripple.png">

Onde:

**Vripple** é o valor de ripple que estamos procurando

**Itotal** é a corrente total

**f** é a frequência do ripple

**C** é a capacitância do capacitor

Movendo um pouco as variáveis, temos que o valorda capacitância é:

<img src="/Imagens/Capacitancia.png">

Sabemos que a frequência da fonte é 60Hz e que ela a frequência do ripple foi dobrada devido à ponte de diodos. O valor do ripple é aproximadamente 16.6 - 14.9. Escolhemos 1.66 para os cálculos.

A corrente desejada é de 100 mA, aproximadamente. Esse cálculo pode ser representado por: 

<img src="/Imagens/Icarga.png">

Icarga é a corrente da carga.

Vzenner é a tensão dada pelo diodo zenner que é 13 - 0.7, devido ao transistor.

Rcarga é a resistência da carga que é 120

Substituindo os valores obtemos:

<img src="/Imagens/corrente_carga_calculo.png">

A corrente total será:

<img src="/Imagens/corrente_total_calculo.png">

Onde: 

<img src="/Imagens/zener_corrente_calculo.png"> 

<img src="/Imagens/corrente_potenciometro_calculo.png">

Logo:

<img src="/Imagens/corrente_total_resultado.png">

Substituindo os valores para encontrar o valor do capacitor obtemos:

<img src="/Imagens/capacitor_calculo_resultado.png">

Portanto, utilizaremos o valor comercial mais próximo que é 680µF.

## Vídeo no Drive

[Vídeo explicando o projeto](https://drive.google.com/file/d/1HcNE9lZL7uafRJnAseCGG7wqgAbMjwtn/view?usp=sharing)

[Slides do Vídeo](https://drive.google.com/file/d/1dZ1GxwcLFC7jLlf4VAwbYwoFwnwfKZ9E/view?usp=sharing)

## Alunos

Bruno Berndt Lima (12542550) - BCC 2021

Henrique Garcia Gomes do Ó (10734331) - BCC 2021

Miguel Reis de Araújo (12752457) - BCC 2021

Pedro Henrique Vilela do Nascimento (12803492) - BCC 2021
